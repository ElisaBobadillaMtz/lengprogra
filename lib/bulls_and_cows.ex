defmodule BullsAndCows do
  def score_guess(secret, guess) do
    secret = String.codepoints(secret)
    guess = String.codepoints(guess)
    numbulls = cuentabulls(secret,guess,0)
    numcows = cuentacows(secret,guess,0)
    numcows = numcows - numbulls
    if numbulls == 4 do
    "You win"
    else
    "#{numbulls} Bulls, #{numcows} Cows"
    end
  end
  def cuentabulls([],[],numbulls) do
    numbulls
  end

  def cuentabulls([h|t1],[h|t2],numbulls)  do
    cuentabulls(t1,t2,numbulls+1)
  end

  def cuentabulls([_h1|t1],[_h2|t2],numbulls) do
    cuentabulls(t1,t2,numbulls)
  end

  def cuentacows(_secret, [],numcows) do
    numcows
  end
  def cuentacows(secret,[h2|t2],numcows) do
    if (Enum.member?(secret,h2)) do
      cuentacows(secret, t2,numcows+1)
    else
      cuentacows(secret,t2,numcows)
    end
  end
end
